#include <iostream>
#include <string>

int main(int argc, const char * argv[]) {
    std::string name;
    name = "Slava";
    std::cout << "value = " << name << "\n";
    std::cout << "length = " << name.length() << "\n";
    std::cout << "first letter = " << name[0] << "\n";
    std::cout << "last letter = " << name[name.length()  - 1] << "\n";
    return 0;
}